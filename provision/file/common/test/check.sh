#!/bin/sh -e

cd "$WORKDIR"

for script in "$WORKDIR"/test.d/*.sh
do . "$script"
done

if ! test -d "$VERSION" || ! test -d chicken-git
then
  echo 1>&2 "You must fetch '$VERSION' and 'chicken-git' first!"
  exit 1
fi

(
  cd "$VERSION"
  export PATH="${MAKEPATH:-$PATH}"

  echo "Building '$PREFIX/$VERSION'..."
  gmake PREFIX="$PREFIX/$VERSION" all

  echo "Installing '$PREFIX/$VERSION'..."
  gmake PREFIX="$PREFIX/$VERSION" install
)

(
  cd chicken-git
  export PATH="${MAKEPATH:-$PATH}"

  if test "$BOOTSTRAP" = "1"
  then
    echo "Building 'boot-chicken'..."
    gmake PREFIX="/nowhere" CHICKEN="$PREFIX/$VERSION/bin/chicken" boot-chicken
  fi

  if test "$BOOTSTRAP" = "1"
  then
    CHICKEN="./chicken-boot"
  else
    CHICKEN="$PREFIX/$VERSION/bin/chicken"
  fi

  echo "Building '$PREFIX/chicken'..."
  gmake PREFIX="$PREFIX/chicken" CHICKEN="$CHICKEN" all

  echo "Installing '$PREFIX/chicken'..."
  gmake PREFIX="$PREFIX/chicken" CHICKEN="$CHICKEN" install

  echo "Testing '$PREFIX/chicken'..."
  gmake PREFIX="$PREFIX/chicken" CHICKEN="$CHICKEN" check
)
