#!/bin/sh -e

cd "$WORKDIR"

for script in "$WORKDIR"/test.d/*.sh
do . "$script"
done

rm -rf "$PREFIX/$VERSION"
rm -rf "$PREFIX/chicken"
rm -rf "$WORKDIR/test.log"

if test -d chicken-git
then
  echo "Resetting to '$REFERENCE'..."
  (
    cd chicken-git
    git clean -dfx
    git checkout "$REFERENCE"
    git reset --hard "$REFERENCE"
  )
fi
