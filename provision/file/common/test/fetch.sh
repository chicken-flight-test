#!/bin/sh -e

cd "$WORKDIR"

for script in "$WORKDIR"/test.d/*.sh
do . "$script"
done

if ! test -d "$VERSION"
then
  echo "Fetching '$RELEASE'..."
  curl -s "$RELEASE" | gunzip - | tar -xf -
  touch "$VERSION"/build-version.c
fi

if ! test -d chicken-git
then
  echo "Fetching '$REPOSITORY'..."
  git clone "$REPOSITORY" chicken-git
else
  echo "Updating '$REPOSITORY'..."
  (
    cd chicken-git
    git remote set-url origin "$REPOSITORY"
    git fetch origin
  )
fi
