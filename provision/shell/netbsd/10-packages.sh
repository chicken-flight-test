#!/bin/sh -e

touch /etc/openssl/openssl.cnf

PKG_PATH="ftp://ftp.NetBSD.org/pub/pkgsrc/packages/NetBSD/$(uname -m)/$(uname -r)/All/" \
  /usr/pkg/bin/pkgin -y install curl git gmake mozilla-rootcerts

PREFIX=/usr/pkg \
  /usr/pkg/sbin/mozilla-rootcerts install
