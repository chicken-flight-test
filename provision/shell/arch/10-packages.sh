#!/bin/sh -e

pacman --noconfirm -Syu
pacman --noconfirm -S --needed curl gcc git make
ln -fns /usr/bin/make /usr/local/bin/gmake
