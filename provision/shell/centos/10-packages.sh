#!/bin/sh -e

yum check-update || test $? -eq 100
yum install -y gcc git make
ln -fns /usr/bin/make /usr/local/bin/gmake
