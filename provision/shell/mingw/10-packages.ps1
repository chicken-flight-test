# Installs packages for mingw.

if ($env:PROCESSOR_ARCHITECTURE -like "amd64") {
  $msys2    = "msys64"
  $mingw    = "mingw64"
  $packages = "mingw-w64-x86_64-gcc mingw-w64-x86_64-make diffutils git tar"
} else {
  $msys2    = "msys32"
  $mingw    = "mingw32"
  $packages = "mingw-w64-i686-gcc mingw-w64-i686-make diffutils git tar"
}

if (-not (test-path "c:\${msys2}\${mingw}\bin\mingw32-make.exe")) {
  start-process "c:/${msys2}/usr/bin/sh.exe" -wait -nonewwindow -argumentlist "-l -c 'pacman --noconfirm -S --needed ${packages}'"
}

if (-not (test-path -path "c:/${msys2}/${mingw}/bin/gmake.exe")) {
  cmd.exe /c mklink "c:\${msys2}\${mingw}\bin\gmake.exe" "c:\${msys2}\${mingw}\bin\mingw32-make.exe"
}
