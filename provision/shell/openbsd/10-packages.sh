#!/bin/sh -e

PKG_PATH="http://ftp.openbsd.org/pub/OpenBSD/$(uname -r)/packages/$(uname -p)/" \
  pkg_add -v curl-- git-- gmake--
