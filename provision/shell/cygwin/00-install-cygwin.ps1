# Fetches and installs cygwin.

$mirror = "http://mirrors.kernel.org/sourceware/cygwin/"

$setup = if ($env:PROCESSOR_ARCHITECTURE -like "amd64") {
  "setup-x86_64"
} else {
  "setup-x86"
}

if (-not (test-path -path "c:/cygwin")) {
  new-item "c:/cygwin" -type directory -force | out-null
}

if (-not (test-path -path "c:/cygwin/setup.exe")) {
  $webclient = new-object system.net.webclient
  $webclient.downloadfile("http://cygwin.com/$setup.exe", "c:/cygwin/setup.exe")
}

start-process "c:/cygwin/setup.exe" -wait -nonewwindow -argumentlist "-dnqv -R c:/cygwin -s $mirror -P curl,gcc-core,git,make"

if (-not (test-path -path "c:/cygwin/bin/gmake.exe")) {
  copy-item "c:/cygwin/bin/make.exe" "c:/cygwin/bin/gmake.exe" -force
}
