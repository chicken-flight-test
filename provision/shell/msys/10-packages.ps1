# Installs packages.

if ($env:PROCESSOR_ARCHITECTURE -like "amd64") {
  $msys2    = "msys64"
  $mingw    = "mingw64"
  $packages = "mingw-w64-x86_64-gcc diffutils git make tar winpty"
} else {
  $msys2    = "msys32"
  $mingw    = "mingw32"
  $packages = "mingw-w64-i686-gcc diffutils git make tar winpty"
}

if (-not (test-path "c:\${msys2}\usr\bin\make.exe")) {
  start-process "c:/${msys2}/usr/bin/sh.exe" -wait -nonewwindow -argumentlist "-l -c 'pacman --noconfirm -S --needed ${packages}'"
}

if (-not (test-path -path "c:/${msys2}/usr/local/bin/gmake.exe")) {
  new-item "c:/${msys2}/usr/local/bin" -type directory -force | out-null
  cmd.exe /c mklink "c:\${msys2}\usr\local\bin\gmake.exe" "c:\${msys2}\usr\bin\make.exe"
}
