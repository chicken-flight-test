# Fetches and installs msys2.

$script = "https://raw.githubusercontent.com/msys2/msys2-installer/master/auto-install.js"

if ($env:PROCESSOR_ARCHITECTURE -like "amd64") {
  $msys2  = "msys64"
  $source = "http://repo.msys2.org/distrib/msys2-x86_64-latest.exe"
} else {
  $msys2  = "msys32"
  $source = "http://repo.msys2.org/distrib/msys2-i686-latest.exe"
}

if (-not (test-path -path "${env:WORKDIR}")) {
  new-item "${env:WORKDIR}" -type directory -force | out-null
}

if (-not (test-path -path "${env:WORKDIR}/msys2.exe")) {
  $webclient = new-object system.net.webclient
  $webclient.downloadfile("$source", "${env:WORKDIR}/msys2.exe")
  $webclient.downloadfile("$script", "${env:WORKDIR}/auto-install.js")
}

if (-not (test-path "c:/${msys2}")) {
  start-process "${env:WORKDIR}/msys2.exe" -wait -nonewwindow -argumentlist "--platform minimal --script ${env:WORKDIR}/auto-install.js --verbose"
  start-process "c:/${msys2}/usr/bin/sh.exe" -wait -nonewwindow -argumentlist "-l -c 'pacman --noconfirm -Syu'"
}
