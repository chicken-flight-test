#!/bin/sh -e

apt-get update
apt-get install -y curl gcc git make
ln -fns /usr/bin/make /usr/local/bin/gmake
