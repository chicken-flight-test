# CHICKEN Flight Test

This is a test harness for [CHICKEN Scheme][1], designed to help check
for build problems across the various platforms it supports. It's
stitched together from [Vagrant][2], shell scripts, and an assortment of
virtual machines found on the [Vagrant Cloud][3].

[1]: https://www.call-cc.org/
[2]: https://www.vagrantup.com/
[3]: https://app.vagrantup.com/boxes/search

## Usage

In short:

    vagrant status              # list available platforms
    vagrant test                # test chicken on all platforms
    vagrant test <machine> ...  # test chicken on specific platforms

### Details

There is a separate virtual machine for each build platform and
architecture. You can list all of them with `vagrant status`.

To create a test machine, run `vagrant up <machine>`. When started for
the first time, it will fetch and install all of CHICKEN's build
dependencies. This may take some time. If anything goes wrong, you can
run `vagrant provision <machine>` to try again.

Once a machine has been created, it can be used to test CHICKEN. The
test process is broken down into three steps:

  1. `fetch` - Fetches two copies of CHICKEN: one from a release tarball
     and one from the core Git repository.
  2. `reset` - Removes build artifacts from any previous test runs and
     resets the Git checkout to the correct revision.
  3. `check` - Builds and tests CHICKEN: first the release version, then
     a bootstrap build from Git, then a full install from Git using the
     bootstrap build, and finally the test suite.

To run these steps in order, run `vagrant test <machine>`. See `vagrant
test --help` for details about the supported options. Test output is
saved to the `logs` directory, named according to the build platform.

### Configuration

If a file called `Vagrantfile.local` exists, it will be loaded and any
changes it makes to the `Test::Env` hash will be put into effect. This
hash controls the test script's environment variables, and can also be
used to adjust the following build variables:

  - `RELEASE`    - The release tarball to use for bootstrapping.
  - `REPOSITORY` - The Git repository from which to fetch CHICKEN.
  - `REFERENCE`  - The Git reference to build (e.g. "master").
  - `BOOTSTRAP`  - Whether to build via bootstrap compiler ("0" or "1").

For example, to test a specific branch or commit, you can set the value
of `Test::Env[:REFERENCE]`:

    Test::Env[:REFERENCE] = 'amazing-feature-branch'

#### Windows

It's possible to test on Windows, but you'll need to provide the Vagrant
boxes yourself. You can create these from the [boxcutter][1] project's
templates for [Windows][2] with the following commands[^1]:

    git clone git://github.com/boxcutter/windows.git boxcutter/windows
    cd boxcutter/windows
    make virtualbox/eval-win10x86-enterprise-ssh
    make virtualbox/eval-win10x64-enterprise-ssh

Building these boxes will take a long time and use a lot of disk space,
but once they have been created you'll be able to use them for testing.

[1]: https://github.com/boxcutter
[2]: https://github.com/boxcutter/windows

[^1]: The commands to create the boxes may have changed since time of
writing, so refer to the project's README for more details.

## Hacking

Patches are very welcome, especially those that add support for more
test platforms.

When setting up a new platform, there are a few things to note:

 - GNU Make must be referred to as `gmake` somewhere on the PATH.
 - Box names generally follow a `flavour-version-arch` convention, where
   `flavour` is used to look up platform-specific configuration files
   under the `provision` directory.
 - Try not to rely on anything but an SSH connection and a shell. This
   includes Vagrant niceties like shared folders.

## Author

Send questions, comments, and patches to Evan Hanson <evhan@foldling.org>.

## License

Public Domain.
