# -*- mode: ruby -*-
# vi: set ft=ruby :

require_relative 'lib/test'

if File.exist? 'Vagrantfile.local'
  load 'Vagrantfile.local'
end

Vagrant.configure('2') do |config|
  #
  # Official boxes from Debian, Ubuntu, and CentOS.
  #

  config.vm.define 'debian-8-amd64' do |box|
    box.vm.box = 'debian/jessie64'
    box.test.platform = 'linux'
  end

  config.vm.define 'debian-9-amd64' do |box|
    box.vm.box = 'debian/stretch64'
    box.test.platform = 'linux'
  end

  config.vm.define 'debian-10-amd64' do |box|
    box.vm.box = 'debian/buster64'
    box.test.platform = 'linux'
  end

  config.vm.define 'ubuntu-16.04-amd64' do |box|
    box.vm.box = 'ubuntu/xenial64'
    box.test.platform = 'linux'
  end

  config.vm.define 'ubuntu-18.04-amd64' do |box|
    box.vm.box = 'ubuntu/bionic64'
    box.test.platform = 'linux'
  end

  config.vm.define 'centos-6-amd64' do |box|
    box.vm.box = 'centos/6'
    box.test.platform = 'linux'
  end

  config.vm.define 'centos-7-amd64' do |box|
    box.vm.box = 'centos/7'
    box.test.platform = 'linux'
  end

  config.vm.define 'centos-8-amd64' do |box|
    box.vm.box = 'centos/8'
    box.test.platform = 'linux'
  end

  #
  # Unofficial boxes, probably not 100% accurate but better than nothing.
  #

  config.vm.define 'arch-amd64' do |box|
    box.vm.box = 'generic/arch'
    box.test.platform = 'linux'
  end

  config.vm.define 'dragonflybsd-5-amd64' do |box|
    box.vm.box = 'generic/dragonflybsd5'
    box.test.platform = 'bsd'
  end

  config.vm.define 'freebsd-11-amd64' do |box|
    box.vm.box = 'generic/freebsd11'
    box.test.platform = 'bsd'
  end

  config.vm.define 'freebsd-12-amd64' do |box|
    box.vm.box = 'generic/freebsd12'
    box.test.platform = 'bsd'
  end

  config.vm.define 'netbsd-8-amd64' do |box|
    box.vm.box = 'generic/netbsd8'
    box.test.platform = 'bsd'
  end

  config.vm.define 'openbsd-6-amd64' do |box|
    box.vm.box = 'generic/openbsd6'
    box.test.platform = 'bsd'
  end

  config.vm.define 'haiku-r1-amd64' do |box|
    box.vm.box = 'haiku-os/r1beta1-x86_64'
    box.test.sudo = false
    box.test.platform = 'haiku'
    box.test.env.merge! LIBRARY_PATH: '/boot/system/lib'
  end

  #
  # The remaining boxes must be built on your own. See README.md for
  # more info about how to do this.
  #

  if Test::Boxes.path.directory?
    config.vm.define 'cygwin-i386' do |box|
      box.vm.box = Test::Boxes['eval-win10x86-enterprise-ssh-nocm-*.box']
      box.ssh.insert_key = false
      box.test.platform = 'cygwin'
      box.test.workdir = 'c:/tmp'
      box.test.shell = 'c:/cygwin/bin/sh.exe -l'
    end

    config.vm.define 'cygwin-amd64' do |box|
      box.vm.box = Test::Boxes['eval-win10x64-enterprise-ssh-nocm-*.box']
      box.ssh.insert_key = false
      box.test.platform = 'cygwin'
      box.test.workdir = 'c:/tmp'
      box.test.shell = 'c:/cygwin/bin/sh.exe -l'
    end

    config.vm.define 'mingw-i386' do |box|
      box.vm.box = Test::Boxes['eval-win10x86-enterprise-ssh-nocm-*.box']
      box.ssh.insert_key = false
      box.test.platform = 'mingw'
      box.test.workdir = 'c:/tmp'
      box.test.shell = 'c:/msys32/usr/bin/sh.exe -l'
      box.test.env.merge! MAKEPATH: '/mingw32/bin:/c/Windows/System32:/c/Windows'
    end

    config.vm.define 'mingw-amd64' do |box|
      box.vm.box = Test::Boxes['eval-win10x64-enterprise-ssh-nocm-*.box']
      box.ssh.insert_key = false
      box.test.platform = 'mingw'
      box.test.workdir = 'c:/tmp'
      box.test.shell = 'c:/msys64/usr/bin/sh.exe -l'
      box.test.env.merge! MAKEPATH: '/mingw64/bin:/c/Windows/System32:/c/Windows'
    end

    config.vm.define 'msys-i386' do |box|
      box.vm.box = Test::Boxes['eval-win10x86-enterprise-ssh-nocm-*.box']
      box.ssh.insert_key = false
      box.test.platform = 'mingw-msys'
      box.test.workdir = 'c:/tmp'
      box.test.shell = 'c:/msys32/usr/bin/sh.exe -l'
      box.test.env.merge! MSYSTEM: 'MINGW32'
    end

    config.vm.define 'msys-amd64' do |box|
      box.vm.box = Test::Boxes['eval-win10x64-enterprise-ssh-nocm-*.box']
      box.ssh.insert_key = false
      box.test.platform = 'mingw-msys'
      box.test.workdir = 'c:/tmp'
      box.test.shell = 'c:/msys64/usr/bin/sh.exe -l'
      box.test.env.merge! MSYSTEM: 'MINGW64'
    end
  end
end
