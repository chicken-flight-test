require 'pathname'
require 'set'
require 'tmpdir'
require 'vagrant'

module Test
  Steps = [:fetch, :reset, :check].freeze

  Env = {
    RELEASE: 'https://code.call-cc.org/releases/current/chicken-5.2.0.tar.gz',
    REPOSITORY: 'git://code.call-cc.org/chicken-core.git',
    REFERENCE: 'master',
    BOOTSTRAP: '1',
  }

  Boxes = Object.new

  class << Boxes
    attr_accessor :path

    def [](name)
      unless path.nil?
        Dir["#{path}/#{name}"].last
      end
    end
  end

  class Plugin < Vagrant.plugin('2')
    name('chicken-flight-test')
    config('test') { Config }
    command('test') { Command }
  end

  class Config < Vagrant.plugin('2', :config)
    # A reference to the Vagrant machine's "box" object.
    attr_accessor :box

    # The test script environment.
    attr_accessor :env

    # The box flavour, e.g. "debian" or "freebsd".
    attr_reader :flavour

    # The CHICKEN build platform.
    attr_accessor :platform

    # Whether to elevate during provisioning.
    attr_accessor :sudo

    # The interpreter to use when executing the test scripts.
    attr_accessor :shell

    # The remote working directory for the test scripts.
    attr_accessor :workdir

    # The local output directory for test execution logs.
    attr_writer :logdir

    def initialize
      @env = Hash[Env]
      @sudo = true
      @platform = 'unknown'
      @shell = '/bin/sh'
      @workdir = '/tmp'
      @logdir = './logs'
    end

    def name=(name)
      @flavour = name.split('-').first
    end

    def logdir
      FileUtils.mkdir_p(@logdir).first
    end

    def prefix
      "#{workdir}/chicken"
    end

    def files
      scan('provision/file/common') +
      scan('provision/file/' << flavour)
    end

    def shells
      scan('provision/shell/common') +
      scan('provision/shell/' << flavour)
    end

    def scan dir
      Pathname.new(dir).find.reject(&:directory?).map(&:to_s)
    rescue Errno::ENOENT
      Array.new
    end

    def provision
      yield self if block_given?

      env[:PLATFORM] ||= platform
      env[:WORKDIR] ||= workdir
      env[:PREFIX] ||= prefix

      files.each do |source|
        destination = source.sub %r{provision/file/[^/]+/}, ''
        box.vm.provision source, type: 'file' do |f|
          f.source = source
          f.destination = File.join workdir, destination
        end
      end

      shells.each do |path|
        box.vm.provision path, type: 'shell' do |s|
          s.env = env
          s.path = path
          s.privileged = sudo
        end
      end
    end
  end

  class Command < Vagrant.plugin('2', :command)
    def Command.synopsis
      'runs the chicken flight tests'
    end

    def execute
      options = {}

      options[:steps] = Set.new(Steps)
      options[:parallel] = false
      options[:provision] = false

      opts = OptionParser.new do |o|
        o.banner = 'Usage: vagrant test [options] [machine ...]'
        o.separator ''
        o.separator 'Options:'
        o.separator ''

        o.on('--[no-]provision', 'Enable or disable provisioning') do |provision|
          options[:provision] = provision
        end

        o.on('--[no-]parallel', 'Enable or disable parallel operation') do |parallel|
          options[:parallel] = parallel
        end

        o.on('--[no-]fetch', 'Run the "fetch" step') do |flag|
          if flag
            options[:steps] |= [:fetch]
          else
            options[:steps] ^= [:fetch]
          end
        end

        o.on('--[no-]reset', 'Run the "reset" step') do |flag|
          if flag
            options[:steps] |= [:reset]
          else
            options[:steps] ^= [:reset]
          end
        end

        o.on('--[no-]check', 'Run the "check" step') do |flag|
          if flag
            options[:steps] |= [:check]
          else
            options[:steps] ^= [:check]
          end
        end
      end

      return unless argv = parse_options(opts)

      names = Array.new.tap do |n|
        with_target_vms(argv) { |m| n << m.name.to_s }
      end

      options[:steps] = Steps & Array(options[:steps])

      if options[:parallel]
        begin
          require 'parallel'
        rescue LoadError => e
          @env.ui.warn 'Could not load "parallel" gem -- tests will not be run in parallel.'
        end
      end

      mapper = if defined?(Parallel)
        Parallel
      else
        Class.new do
          def self.map(ary, *args, &block)
            ary.map(&block)
          end
        end
      end

      results = mapper.map(names) do |name|
        machine = vm(name)
        box = machine.config.test.box

        test = box.test
        state = machine.state.id
        connection = machine.communicate
        exports = test.env.map { |k, v| "export #{k}=#{v}\n" }
        logger = Logger.new("#{test.logdir}/#{name}.log")

        begin
          machine.action(:up) unless state == :running
          machine.action(:provision) if options[:provision]
          options[:steps].each do |step|
            Tempfile.create do |script|
              script.puts "#!/bin/sh"
              script.puts "#{exports.join}"
              script.puts "exec #{test.workdir}/test/#{step}.sh"
              script.chmod 0755
              script.close
              connection.upload(script.path, "#{test.workdir}/run.sh")
              connection.execute("#{test.shell} -e #{test.workdir}/run.sh", elevated: true) do |type, data|
                case type
                when :stdout
                  machine.ui.detail(data.chomp, color: :default)
                  logger.info(data.chomp)
                else
                  machine.ui.detail(data.chomp, color: :red)
                  logger.warn(data.chomp)
                end
              end
            end
          end
        rescue Exception => e
          machine.ui.warn e.message.sub(/./, &:upcase)
          false
        ensure
          case state
          when :not_created
            machine.action(:destroy, force_confirm_destroy: true)
          when :running
            # leave it running
          else
            machine.action(:suspend)
          end
        end
      end

      names.zip(results).each do |name, success|
        if success
          vm(name).ui.info 'Success!'
        else
          vm(name).ui.error 'Failure!'
        end
      end

      failures = results.reject { |success| success }

      failures.size
    end

    def vm(name)
      with_target_vms([name]) { |m| return m }
    end
  end
end
