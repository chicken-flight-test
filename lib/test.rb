require 'pathname'

require_relative 'test/plugin'

Test::Boxes.path = Pathname.new('boxcutter/windows/box/virtualbox')

module Vagrant
  module UI
    class Basic
      def report_progress(*args)
        # disabled to avoid interleaving output
      end
    end
  end
end

class << Vagrant
  alias default_configure configure

  def configure(version, &block)
    default_configure(version) do |config|
      config.vm.synced_folder '.', '/vagrant', disabled: true

      class << config.vm
        alias default_define define

        def define(name)
          default_define(name) do |box|
            box.test.box = box
            box.test.name = name
            box.test.provision do
              yield box if block_given?
            end
          end
        end
      end

      block.call(config)
    end
  end
end
